# Work locally

Build Service

```
./gradlew clean build
```

Execute dependencies with Docker Compose and this service with Gradle:

```
docker-compose --file docker-compose.dependencies.yml build
docker-compose --file docker-compose.dependencies.yml up
./gradlew bootRun
```

Execute everything with Docker Compose

```
docker-compose --file docker-compose.dependencies.yml --file docker-compose.this.yml up --build --force-recreate --remove-orphans
```

Test

```
for file in src/test/*postman_collection.json; do newman run $file --environment src/test/*-local.postman_environment.json --reporters cli,html --reporter-html-export "newman-results-$(basename $file .json).html"; done
```

## API documentation

On©line API Documentation is at:

- HTML: http://localhost:8450/price-orchestration/v1/swagger-ui.html
- JSON: http://localhost:8450/price-orchestration/v1/api-docs

Offline API Documentation is at https://velox-shop.gitlab.io/price-orchestration/