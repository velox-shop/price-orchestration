package shop.velox.price_orchestration.service.impl;

import java.net.URI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import shop.velox.price.api.dto.CurrencyDto;

import shop.velox.price_orchestration.service.PriceOrchestrationService;

@Service
public class PriceOrchestrationServiceImpl implements PriceOrchestrationService {

  private static final Logger LOG = LoggerFactory.getLogger(PriceOrchestrationServiceImpl.class);

  private RestTemplate restTemplate;

  @Value("${price.url}")
  private String priceUrl;

  public PriceOrchestrationServiceImpl(@Autowired final RestTemplate restTemplate) {
    this.restTemplate = restTemplate;
  }
  @Override
  public ResponseEntity<CurrencyDto> getCurrency(String id) {
    URI uri = UriComponentsBuilder.fromUriString(priceUrl)
        .path("/currencies/")
        .path(id)
        .build()
        .toUri();
    ResponseEntity<CurrencyDto> currencyDto = restTemplate.getForEntity(uri, CurrencyDto.class);
    LOG.info("getCurrency status: {},  body: {}", currencyDto.getStatusCode(),
        currencyDto.getBody());
    return currencyDto;

  }

  @Override
  public ResponseEntity<String> getCurrencies(Pageable pageable) {
    UriComponentsBuilder uriComponentsBuilder = UriComponentsBuilder.fromUriString(priceUrl)
        .path("/currencies")
        .queryParam("page", pageable.getPageNumber())
        .queryParam("size", pageable.getPageSize());

    pageable.getSort().get()
        .forEach(sort -> uriComponentsBuilder.queryParam("sort",
            sort.getProperty() + "," + sort.getDirection()));
    URI targetUrl = uriComponentsBuilder
        .build()
        .toUri();

    ResponseEntity<String> responseEntity = restTemplate.exchange(targetUrl, HttpMethod.GET,
        new HttpEntity<>(pageable), new ParameterizedTypeReference<>() {
        });
    LOG.debug("{}, {}", responseEntity.getStatusCode(), responseEntity.getBody());

    return ResponseEntity.status(responseEntity.getStatusCode()).body(responseEntity.getBody());
  }

 }