package shop.velox.price_orchestration.service;

import java.math.BigDecimal;
import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import shop.velox.price.api.dto.ArticleListDto;
import shop.velox.price.api.dto.CurrencyDto;
import shop.velox.price.api.dto.PriceDto;
import shop.velox.price.api.dto.PriceStatus;

public interface PriceOrchestrationService {

  ResponseEntity<CurrencyDto> getCurrency(String id);

  ResponseEntity<String> getCurrencies(Pageable pageable);
}