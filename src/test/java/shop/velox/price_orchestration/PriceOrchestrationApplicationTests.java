package shop.velox.price_orchestration;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles({"localauth"})
class PriceOrchestrationApplicationTests {

	@Test
	void contextLoads() {
	}

}
